package com.agileai.crm.module.province.handler;

import com.agileai.crm.module.province.service.ProvinceListSelect;
import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.PickFillModelHandler;

public class ProvinceListSelectListHandler
        extends PickFillModelHandler {
    public ProvinceListSelectListHandler() {
        super();
        this.serviceId = buildServiceId(ProvinceListSelect.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "codeName", "");
    }

    protected ProvinceListSelect getService() {
        return (ProvinceListSelect) this.lookupService(this.getServiceId());
    }
}
