<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String currentSubTableId = pageBean.getStringValue("currentSubTableId");
String currentSubTableIndex = pageBean.getStringValue("currentSubTableIndex");
%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
var operaRequestBox;
function openContentRequestBox(operaType,title,handlerId,subPKField){
	if ('insert' != operaType && !isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!operaRequestBox){
		operaRequestBox = new PopupBox('operaRequestBox',title,{size:'big',top:'3px'});
	}
	var columnIdValue = $("#columnId").val();
	var url = 'index?'+handlerId+'&TC_ID='+columnIdValue+'&operaType='+operaType+'&'+subPKField+'='+$("#"+subPKField).val();
	operaRequestBox.sendRequest(url);
}
function showFilterBox(){
	$('#filterBox').show();
	var clientWidth = $(document.body).width();
	var tuneLeft = (clientWidth - $("#filterBox").width())/2-2;	
	$("#filterBox").css('left',tuneLeft);	
}
function doRemoveContent(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	jConfirm('确认要移除该条记录吗？',function(r){
		if(r){
			postRequest('form1',{actionType:'isLastRelation',onComplete:function(responseText){
				if (responseText == 'true'){
					jConfirm('该信息只有一条关联记录，确认要删除吗？',function(r){
						if(r){
							doSubmit({actionType:'delete'});
						}
					});
				}else{
					doSubmit({actionType:'removeContent'});
				}
			}});
		}
	});
}

function isSelectedTree(){
	if (isValid($('#columnId').val())){
		return true;
	}else{
		return false;
	}
}
function refreshTree(){
	doQuery();
}
function refreshContent(curNodeId){
	if (curNodeId){
		$('#columnId').val(curNodeId);
	}
	doSubmit({actionType:'query'});
}
var targetTreeBox;
function openTargetTreeBox(curAction){
	var columnIdValue = $("#columnId").val();
	if (!isSelectedTree()){
		writeErrorMsg('请先选中一个树节点!');
		return;
	}
	if (curAction == 'copyContent' || curAction == 'moveContent'){
		if (!isSelectedRow()){
			writeErrorMsg('请先选中一条记录!');
			return;
		}
		columnIdValue = $("#curColumnId").val()
	}	
	if (!targetTreeBox){
		targetTreeBox = new PopupBox('targetTreeBox','请选择目标分组',{size:'normal',width:'300px',top:'3px'});
	}
	var handlerId = "TaskCycle8ContentManageTreePick";
	var url = 'index?'+handlerId+'&TC_ID='+columnIdValue;
	targetTreeBox.sendRequest(url);
	$("#actionType").val(curAction);
}
function doChangeParent(){
	var curAction = $('#actionType').val();
	postRequest('form1',{actionType:curAction,onComplete:function(responseText){
		if (responseText == 'success'){
			if (curAction == 'moveTree'){
				refreshTree();			
			}else{
				refreshContent($("#targetParentId").val());		
			}
		}else {
			writeErrorMsg('迁移父节点出错啦！');
		}
	}});
}
function clearFilter(){
	$("#filterBox input[type!='button'],select").val('');
}
function changeSubTable(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'changeSubTable'});
	parent.changeTabId(subTableId);
}
var orgIdBox;
function openProIdBox(){
	var handlerId = "TaskCycleSelect"; 
	if (!orgIdBox){
		orgIdBox = new PopupBox('orgIdBox','请选择周期',{size:'normal',width:'300',top:'2px'});
	}
	var url = 'index?'+handlerId+'&targetId=TC_ID';
	orgIdBox.sendRequest(url);
}
function generatPlan(){
	var confirmMsg = '"您确认要选择当前周期吗？';
	jConfirm(confirmMsg,function(r){
		if(r){
			doSubmit({actionType:'generatPlan'});
		}else{
			doSubmit({actionType:'prepareDisplay'});
		}
	});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table width="100%" style="margin:0px;">
<tr>
	<td width="85%" valign="top">
<div style="">
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div class="photobg1" id="tabHeader">
	<div class="newarticle1" onclick="changeSubTable('ColdCalls')">陌生拜访</div>
	<div class="newarticle1" onclick="changeSubTable('FollowUp')">意向跟进</div>
	<div class="newarticle1" onclick="changeSubTable('WorkSummary')">工作总结</div>
</div>

<div class="photobox newarticlebox" id="Layer0" style="height:auto;display:none;overflow:hidden;">
<%
if("ColdCalls".equals(pageBean.inputValue("currentSubTableId"))){
%>
<iframe id="HandlerFrame" src="index?ReviewColdCallsList&TC_ID=<%=pageBean.inputValue("TC_ID")%>&SALE_ID=<%=pageBean.inputValue("SALE_ID")%>" width="100%" height="450" frameborder="0" scrolling="no"></iframe>
<%} %>
</div>
<div class="photobox newarticlebox" id="Layer1" style="height:auto;display:none;overflow:hidden;">
<%
if("FollowUp".equals(pageBean.inputValue("currentSubTableId"))){
%>
<iframe id="HandlerFrame" src="index?ReviewFollowUpList&TC_ID=<%=pageBean.inputValue("TC_ID")%>&SALE_ID=<%=pageBean.inputValue("SALE_ID")%>" width="100%" height="450" frameborder="0" scrolling="no"></iframe>
<%} %>
</div>
<div class="photobox newarticlebox" id="Layer2" style="height:auto;display:none;overflow:hidden;">
<%
if("WorkSummary".equals(pageBean.inputValue("currentSubTableId"))){
%>
<iframe id="HandlerFrame" src="index?ReviewSummary&TC_ID=<%=pageBean.inputValue("TC_ID")%>&SALE_ID=<%=pageBean.inputValue("SALE_ID")%>&TC_BEGIN=<%=pageBean.inputValue("TC_BEGIN")%>&TC_END=<%=pageBean.inputValue("TC_END")%>" width="100%" height="450" frameborder="0" scrolling="no"></iframe>
<%} %>
</div>
</div>
</td>
</tr>
</table>
<script language="javascript">
var tab = new Tab('tab','tabHeader','Layer',0);
tab.focus({"id":"<%=pageBean.inputValue("currentLayerleId")%>"});
$(function(){
	resetTreeHeight(78);	
	resetRightAreaHeight(87);
});
</script>
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" id="currentSubTableId" name="currentSubTableId" value="<%=pageBean.inputValue("currentSubTableId")%>" />
<input type="hidden" name="TC_ID" id="TC_ID" value="<%=pageBean.inputValue("TC_ID")%>"/>
<input type="hidden" name="SALE_ID" id="SALE_ID" value="<%=pageBean.inputValue("SALE_ID")%>"/>
<input type="hidden" name="TC_BEGIN" id="TC_BEGIN" value="<%=pageBean.inputValue("TC_BEGIN")%>"/>
<input type="hidden" name="TC_END" id="TC_END" value="<%=pageBean.inputValue("TC_END")%>"/>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
