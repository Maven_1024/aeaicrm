package com.agileai.crm.module.procustomer.handler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.agileai.crm.common.PrivilegeHelper;
import com.agileai.crm.cxmodule.OrgInfoManage;
import com.agileai.crm.cxmodule.ProcustVisitManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class OrgInfoManageListHandler extends StandardListHandler {
	public OrgInfoManageListHandler() {
		super();
		this.editHandlerClazz = OrgInfoManageEditHandler.class;
		this.serviceId = buildServiceId(OrgInfoManage.class);
	}

	public ViewRenderer prepareDisplay(DataParam param) {
		User user = (User) getUser();
		PrivilegeHelper privilegeHelper = new PrivilegeHelper(user);
		if(privilegeHelper.isSalesDirector()){
			setAttribute("doOpera", true);
		}else{
			param.put("orgSalesman", user.getUserId());
		}
		setAttribute("doEdit8Save", true);
		setAttribute("doDel", true);
		String ORG_LABELS = param.get("orgLabels");
		String sqlSyntax = " ";
		
		if (!StringUtil.isNullOrEmpty(ORG_LABELS)){
			if ("Unlabeled".equals(ORG_LABELS)){
				sqlSyntax = " and (a.ORG_LABELS is null or a.ORG_LABELS = '')";
			}
			else if ("Marked".equals(ORG_LABELS)){
				sqlSyntax = " and (a.ORG_LABELS is not null and a.ORG_LABELS != '')";
			}else{
				sqlSyntax = " and a.ORG_LABELS='"+ORG_LABELS+"'";
			}
		}
		param.put("sqlSyntax",sqlSyntax);
		
		String orgSalesman = param.get("orgSalesman");
		String saleSqlSyntax = " ";
		if("Unallocated".equals(orgSalesman)){
			saleSqlSyntax = " and (a.ORG_SALESMAN is null or a.ORG_SALESMAN = '')";
		}else if(StringUtil.isNotNullNotEmpty(orgSalesman)){
			saleSqlSyntax = " and a.ORG_SALESMAN = '"+param.get("orgSalesman")+"'";
		}
		param.put("saleSqlSyntax",saleSqlSyntax);
		
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().findRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	
	protected void processPageAttributes(DataParam param) {
		initMappingItem("ORG_STATE", 
				FormSelectFactory.create("PER_STATE").getContent());
		initMappingItem("ORG_CLASSIFICATION", 
				FormSelectFactory.create("ORG_CLASSIFICATION").getContent());
		setAttribute("orgState", FormSelectFactory.create("PER_STATE")
				.addSelectedValue(param.get("orgState")));
		setAttribute("orgClassification", FormSelectFactory.create("ORG_CLASSIFICATION")
				.addSelectedValue(param.get("orgClassification")));
		
		FormSelect orgLavelsFormSelect = new FormSelect();
		List<DataRow> records = getService().findOrgLabelsRecords(param);
		DataRow unlabeled = new DataRow("CODE_ID","Unlabeled","CODE_NAME","未标记");
		DataRow marked = new DataRow("CODE_ID","Marked","CODE_NAME","已标记");
		records.add(0, unlabeled);
		records.add(1, marked);
		orgLavelsFormSelect.setKeyColumnName("CODE_ID");
		orgLavelsFormSelect.setValueColumnName("CODE_NAME");
		orgLavelsFormSelect.putValues(records);
		String selectedValue = this.getAttributeValue("orgLabels","");
		setAttribute("orgLabels", orgLavelsFormSelect.addSelectedValue(selectedValue));
		
		FormSelect orgSalesmanFormSelect = new FormSelect();
		List<DataRow> salesmanrecords = getService().findOrgSalesmanRecords(param);
		orgSalesmanFormSelect.setKeyColumnName("USER_ID");
		orgSalesmanFormSelect.setValueColumnName("USER_NAME");
		orgSalesmanFormSelect.putValues(salesmanrecords);
		String salesmanSelectedValue = this.getAttributeValue("orgSalesman","");
		setAttribute("orgSalesman", orgSalesmanFormSelect.addSelectedValue(salesmanSelectedValue));
	}

	protected void initParameters(DataParam param) {
		initParamItem(param, "orgState", "0");
		initParamItem(param, "ORG_TYPE", "");
		initParamItem(param, "ORG_CREATER_NAME", "");
		initParamItem(param, "sdate", "");
		initParamItem(
				param,
				"edate",
				DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,
						DateUtil.getDateAdd(new Date(), DateUtil.DAY, 1)));
	}
	
	public ViewRenderer doAssignedSaleAction(DataParam param){
		String ids = param.get("ids");
    	String[] idArray = ids.split(",");
    	for(int i=0;i < idArray.length;i++){
        	String id = idArray[i];
        	String orgSalesman = param.get("ORG_SALESMAN");
        	DataParam idParam = new DataParam();
        	idParam.put("ORG_ID", id);
        	idParam.put("ORG_SALESMAN", orgSalesman);
        	getService().assignedSaleRecord(idParam);
    	}
		return prepareDisplay(param);
	}
	
	public ViewRenderer doBatchDeleteAction(DataParam param){
		String responseText = "";
		ProcustVisitManage service = this.lookupService(ProcustVisitManage.class);
		List<DataParam> paramList = new ArrayList<DataParam>();
		String ids = param.get("ids");
		String orgNames = "";
		String[] idArray = ids.split(",");
    	for(int i=0;i < idArray.length;i++){
    		String id = idArray[i];
    		DataParam idParam = new DataParam();
    		idParam.put("ORG_ID", id);
    		List<DataRow> rsList = service.findRecords(idParam);
    		if(rsList.isEmpty()){
    			paramList.add(idParam);
    		}else{
    			DataRow record = getService().getRecord(idParam);
    			orgNames = orgNames+record.get("ORG_NAME")+"\n";
    			responseText = orgNames+"存在拜访记录不能删除";
    		}
    	}
    	getService().batchDeleteRecords(paramList);
    	return new AjaxRenderer(responseText);
	}

	protected OrgInfoManage getService() {
		return (OrgInfoManage) this.lookupService(this.getServiceId());
	}

}
